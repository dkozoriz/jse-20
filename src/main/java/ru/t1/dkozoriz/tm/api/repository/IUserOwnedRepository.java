package ru.t1.dkozoriz.tm.api.repository;

import ru.t1.dkozoriz.tm.model.UserOwnedModel;

import java.util.List;

public interface IUserOwnedRepository<T extends UserOwnedModel> extends IAbstractRepository<T>{

    void clear(String userId);

    boolean existById(String userId, String id);

    List<T> findAll(String userId);

    T findById(String userId, String id);

    T findByIndex(String userId, Integer index);

    int getSize(String userId);

    T removeById(String userId, String id);

    T removeByIndex(String userId, Integer index);

    T add(String userId, T model);

    T remove(String userId, T model);

}
