package ru.t1.dkozoriz.tm.api.service;

import ru.t1.dkozoriz.tm.model.business.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(String userId, String projectId, String taskId);

    void removeProjectById(String userId, String projectId);

    Task unbindTaskToProject(String userId, String projectId, String taskId);

}