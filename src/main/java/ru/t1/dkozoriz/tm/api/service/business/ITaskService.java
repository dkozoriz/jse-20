package ru.t1.dkozoriz.tm.api.service.business;

import ru.t1.dkozoriz.tm.model.business.Task;

import java.util.List;

public interface ITaskService extends IBusinessService<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);

    Task create(String userId, String name, String description, String projectId);

}