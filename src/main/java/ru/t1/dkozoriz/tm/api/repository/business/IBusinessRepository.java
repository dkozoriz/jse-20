package ru.t1.dkozoriz.tm.api.repository.business;

import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.api.repository.IUserOwnedRepository;
import ru.t1.dkozoriz.tm.model.business.BusinessModel;

import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<T extends BusinessModel> extends IUserOwnedRepository<T> {

    List<T> findAll(String userId, Comparator<? super IWBS> comparator);

}