package ru.t1.dkozoriz.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
