package ru.t1.dkozoriz.tm.model.business;

import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.enumerated.Status;

public final class Task extends BusinessModel implements IWBS {

    private String projectId;

    public Task() {
        super();
    }

    public Task(final String name) {
        super(name);
    }

    public Task(final String name, final Status status) {
        super(name, status);
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

}