package ru.t1.dkozoriz.tm.command.system;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    private static final String NAME = "exit";

    private static final String DESCRIPTION = "close application.";

    public String getName() {
        return NAME;
    }

    public String getArgument() {
        return null;
    }

    public String getDescription() {
        return DESCRIPTION;
    }

    public void execute() {
        System.exit(0);
    }

}