package ru.t1.dkozoriz.tm.command.task;

public final class TaskListClearCommand extends AbstractTaskCommand {

    private static final String NAME = "task-clear";

    private static final String DESCRIPTION = "delete all tasks.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        final String userId = getUserId();
        getTaskService().clear(userId);
    }

}
