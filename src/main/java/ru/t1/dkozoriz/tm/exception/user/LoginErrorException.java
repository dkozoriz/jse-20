package ru.t1.dkozoriz.tm.exception.user;

import ru.t1.dkozoriz.tm.exception.AbstractException;

public final class LoginErrorException extends AbstractException {

    public LoginErrorException() {
        super("Error! Login or password is incorrect.");
    }

}
