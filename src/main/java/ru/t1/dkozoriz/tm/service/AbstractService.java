package ru.t1.dkozoriz.tm.service;

import ru.t1.dkozoriz.tm.api.repository.IAbstractRepository;
import ru.t1.dkozoriz.tm.api.service.IAbstractService;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.model.AbstractModel;

import java.util.List;

public abstract class AbstractService<T extends AbstractModel, R extends IAbstractRepository<T>> implements IAbstractService<T> {

    protected final R repository;

    protected abstract String getName();

    public AbstractService(final R abstractRepository) {
        this.repository = abstractRepository;
    }

    public T add(final T model) {
        if (model == null) throw new EntityException(getName());
        return repository.add(model);
    }

    public void clear() {
        repository.clear();
    }

    public List<T> findAll() {
        return repository.findAll();
    }

    public T remove(final T model) {
        if (model == null) throw new EntityException(getName());
        return repository.remove(model);
    }

    public T removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final T model = findById(id);
        return remove(model);
    }

    public T findById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        T model = repository.findById(id);
        if (model == null) throw new EntityException(getName());
        return model;
    }

    public int getSize() {
        return repository.getSize();
    }

}